const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const ProfileSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  subtopic: {
    type: String
  },
  website: {
    type: String
  },
  location: {
    type: String
  },
  topic: {
    type: String,
  },
  interests: {
    type: [String],
  },
  bio: {
    type: String
  },
  stream: [
    {
      title: {
        type: String,
        required: true,
      },
      streams: {
        type: String,
      },
      description: {
        type: String,
        required: true
      }
    }
],
  social: {
    youtube: {
      type: String
    },
    twitter: {
      type: String
    },
    facebook: {
      type: String
    },
    linkedin: {
      type: String
    },
    instagram: {
      type: String
    }
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Profile = mongoose.model('profile', ProfileSchema);
