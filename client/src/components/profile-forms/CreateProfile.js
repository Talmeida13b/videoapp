import React, { useEffect, useState, Fragment } from 'react';
import { Link, withRouter, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createProfile, getCurrentProfile } from '../../actions/profile';


//create profile form view
const CreateProfile = ({
  createProfile,
  getCurrentProfile,
  profile: { profile, loading },
  history
}) => {
  const [formData, setFormData] = useState({
    subtopic: '',
    website: '',
    location: '',
    topic: '',
    interests: '',
    bio: '',
    twitter: '',
    facebook: '',
    linkedin: '',
    youtube: '',
    instagram: ''
  });

  const [displaySocialInputs, toggleSocialInputs] = useState(false);

  const {
    subtopic,
    website,
    location,
    topic,
    interests,
    bio,
    twitter,
    facebook,
    linkedin,
    youtube,
    instagram
  } = formData;


  const onChange = e =>
    setFormData({ ...formData, [e.target.name]: e.target.value });


  const onSubmit = e => {
    e.preventDefault();
    createProfile(formData, history);
  };


  useEffect(() => {
    getCurrentProfile();
  }, [getCurrentProfile]);


  return loading && profile === null ? (
    <Redirect to="/dashboard" />
  ) : (
    <Fragment>
      <h1 className="large text-light">Create Your Profile</h1>
      <p className="lead">
        <i className="fas fa-user" /> What Should We Know?
      </p>
      <small>* = required field</small>
      <form className="form" onSubmit={onSubmit}>
        <div className="form-group">
          <select name="topic" value={topic} onChange={onChange}>
            <option value="0">* Select Your Topic</option>
            <option value="Enviromental">Enviromental</option>
            <option value="Medical">Medical</option>
            <option value="Politics">Politics</option>
            <option value="Technology">Technology</option>
            <option value="Cultures">Cultures</option>
            <option value="Sports">Sports</option>
            <option value="Video Games">Video Games</option>
            <option value="Music">Music</option>
            <option value="Other">Other</option>
          </select>
          <small className="form-text">
            Tell us what you are going to be streaming about?
          </small>
        </div>
        <div className="form-group">
          <input
            type="text"
            placeholder="Sub Topics"
            name="subtopic"
            value={subtopic}
            onChange={onChange}
          />
          <small className="form-text">
            Any other topic you will be streaming about?
          </small>
        </div>
        <div className="form-group">
          <input
            type="text"
            placeholder="Website"
            name="website"
            value={website}
            onChange={onChange}
          />
          <small className="form-text">
            Do you have a website?
          </small>
        </div>
        <div className="form-group">
          <input
            type="text"
            placeholder="Location"
            name="location"
            value={location}
            onChange={onChange}
          />
          <small className="form-text">
            City & state suggested (eg. Boston, MA)
          </small>
        </div>
        <div className="form-group">
          <input
            type="text"
            placeholder="* Other Interests"
            name="interests"
            value={interests}
            onChange={onChange}
          />
          <small className="form-text">
            What are topics of interest to you to connect you with other like minded people
          </small>
        </div>
        <div className="form-group">
          <textarea
            placeholder="A short bio of yourself"
            name="bio"
            value={bio}
            onChange={onChange}
          />
          <small className="form-text">Tell us a little about yourself</small>
        </div>

        <div className="my-2">
          <button
            onClick={() => toggleSocialInputs(!displaySocialInputs)}
            type="button"
            className="btn btn-light"
          >
            Add Social Network Links
          </button>
          <span>Optional</span>
        </div>
        {displaySocialInputs && (
          <Fragment>
            <div className="form-group social-input">
              <i className="fab fa-twitter fa-2x" />
              <input
                type="text"
                placeholder="Twitter URL"
                name="twitter"
                value={twitter}
                onChange={onChange}
              />
            </div>

            <div className="form-group social-input">
              <i className="fab fa-facebook fa-2x" />
              <input
                type="text"
                placeholder="Facebook URL"
                name="facebook"
                value={facebook}
                onChange={onChange}
              />
            </div>

            <div className="form-group social-input">
              <i className="fab fa-youtube fa-2x" />
              <input
                type="text"
                placeholder="YouTube URL"
                name="youtube"
                value={youtube}
                onChange={onChange}
              />
            </div>

            <div className="form-group social-input">
              <i className="fab fa-linkedin fa-2x" />
              <input
                type="text"
                placeholder="Linkedin URL"
                name="linkedin"
                value={linkedin}
                onChange={onChange}
              />
            </div>

            <div className="form-group social-input">
              <i className="fab fa-instagram fa-2x" />
              <input
                type="text"
                placeholder="Instagram URL"
                name="instagram"
                value={instagram}
                onChange={onChange}
              />
            </div>
          </Fragment>
        )}

        <input type="submit" className="btn btn-primary my-1" />
        <Link className="btn btn-light my-1" to="/dashboard">
          Go Back
        </Link>
      </form>
    </Fragment>
  );
};


CreateProfile.propTypes = {
  createProfile: PropTypes.func.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired
};


const mapStateToProps = state => ({
  profile: state.profile
});


export default connect(
  mapStateToProps, 
  { createProfile, getCurrentProfile })
  (withRouter(CreateProfile)
);
