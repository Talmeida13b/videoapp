import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Spinner from '../layout/Spinner';
import ProfileTop from './ProfileTop';
import ProfileAbout from './ProfileAbout';
import ProfileStream from './ProfileStream';
import { getProfileById } from '../../actions/profile';
import PostForm from '../posts/PostForm';
import Posts from '../posts/Posts'
import Payment from '../layout/Payment';



//displays a user profile after selecting on developers page
const Profile = ({
  getProfileById,
  profile: { profile, loading },
  auth,
  match }) => 
  {
  useEffect(() => {
    getProfileById(match.params.id);
  }, [getProfileById, match.params.id]);


  return (
    <Fragment>
      {profile === null || loading ? (
        <Spinner />
      ) : (
        <Fragment>
          <Link to='/profiles' className='btn btn-light'>
            Back To Profiles
          </Link>
          <span key="1"><Payment /></span>
          {auth.isAuthenticated &&
            auth.loading === false &&
            auth.user._id === profile.user._id && (
              <Link to='/edit-profile' className='btn btn-dark'>
                Edit Profile
              </Link>
            )}
          <div className='profile-grid my-1'>
            <ProfileTop profile={profile} />
            <ProfileAbout profile={profile} />
            
            <div className='profile-exp bg-white p-2'>
              <h2 className='text-danger'>Videos</h2>
              {profile.stream.length > 0 ? (
                <Fragment>
                  {profile.stream.map(stream => (
                    <ProfileStream
                      key={stream._id}
                      stream={stream}
                    />
                  ))}
                </Fragment>
              ) : (
                <h4>No Streams Recorded</h4>
              )}
            </div>

            <div>
              <PostForm />
              <Posts />
            </div>

          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

Profile.propTypes = {
  getProfileById: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { getProfileById }
)(Profile);

