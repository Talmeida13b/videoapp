import React from 'react';
import PropTypes from 'prop-types';



const ProfileStream = ({
  stream: { streams, title, description }
}) => (
  <div>
      <p className="text-dark">
        <strong>Title: </strong> {title}
      </p>
      <p className="text-dark">
        <strong>Description: </strong> {description}
      </p>
      <div className="embed-responsive embed-responsive-16by9">
        <iframe className="embed-responsive-item" src={`https://www.youtube.com/embed/${streams}`} allowfullscreen></iframe>
      </div>
  </div>
);


ProfileStream.propTypes = {
  stream: PropTypes.object.isRequired
};


export default ProfileStream;


//for link connection
/*<p>
    <strong>Stream: </strong> <a href={streams}>Link</a>
  </p>*/