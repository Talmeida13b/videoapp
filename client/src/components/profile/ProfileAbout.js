import React, { Fragment } from 'react';
import PropTypes from 'prop-types';


const ProfileAbout = ({
  profile: {
    bio,
    interests,
    user: { name }
  }
}) => (
  <div className='profile-about bg-dark p-2'>
    {bio && (
      <Fragment>
        <h2 className='text-light'>{name.trim().split(' ')[0]}'s Bio</h2>
        <p>{bio}</p>
        <div className='line' />
      </Fragment>
    )}

    <h2 className='text-light'>Interests</h2>

    <div className='skills'>
      {interests.map((interest, index) => (
        <div key={index} className='p-1'>
          <i className='fas fa-check' /> {interest}
        </div>
      ))}

    </div>
  </div>
);


ProfileAbout.propTypes = {
  profile: PropTypes.object.isRequired
};


export default ProfileAbout;
