import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';


//this displays the profile data on list(not individual)
const ProfileItem = ({
  profile: {
    user: { _id, name, avatar },
    topic,
    subtopic,
    location,
    interests
  }
}) => {
  return (
    <div className='profile bg-dark'>
      <img src={avatar} alt='' className='round-img' />
      <div>
        <h2>{name}</h2>
        <p>
          Topic: {topic} {subtopic && <span> specifically about {subtopic}</span>}
        </p>
        <p className='my-1'>
          {location && <span>{location}</span>}
        </p>
        <Link to={`/profile/${_id}`} className='btn btn-primary'>
          View Profile
        </Link>
      </div>
      <ul>
        {interests.slice(0, 4).map((interest, index) => (
          <li key={index} className='text-danger'>
            <i className='fas fa-check' /> {interest}
          </li>))
        }
      </ul>
    </div>
  );
};


ProfileItem.propTypes = {
  profile: PropTypes.object.isRequired
};


export default ProfileItem;
