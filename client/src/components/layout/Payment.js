import React, { Component } from 'react';
import StripeCheckout from 'react-stripe-checkout';
import { connect } from 'react-redux';
import { handleToken } from '../../actions/profile';

class Payment extends Component {
  render() {
    return (
      <StripeCheckout
        name="Xtreamer"
        description="Support This Xtreamer"
        amount={200}
        //token={token => this.props.handleToken(token)}
        token={token => console.log(token)}
        stripeKey={process.env.REACT_APP_STRIPE_KEY}
      >
        <button className="btn btn-success">
          Donate
        </button>
      </StripeCheckout>
    );
  }
}


export default connect(null, handleToken)(Payment);


