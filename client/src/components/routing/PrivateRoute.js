import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

//PrivateRoute is makes it that those routes are only accessible to authenticated users
//function verifies if user is authtenticated and not loading
//if user is not authenticated redirect when trying to access protected routesto log in
const PrivateRoute = ({ 
  component: Component, 
  auth: { isAuthenticated }, 
  ...rest 
}) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated ? <Component {...props} /> : <Redirect to="/login" />
    }
  />
);

PrivateRoute.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps)(PrivateRoute);
