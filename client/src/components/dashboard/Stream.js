import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteStream } from '../../actions/profile';

//shows streams created on dashboard
const Stream = ({ stream, deleteStream }) => {

  const streamList = stream.map(stream => (
    <tr key={stream._id}>
      <td>{stream.title}</td>
      <td className="hide-sm">{stream.description}</td>
      <td>
        <button onClick={() => deleteStream(stream._id)} className="btn btn-danger">
          Delete
        </button>
      </td>
    </tr>
  ));

  return (
    <Fragment>
      <h2 className="my-2">Recorded Streams</h2>
      <table className="table">
        <thead>
          <tr>
            <th className='text-light'>Title</th>
            <th className="hide-sm text-light">Description</th>
            <th className="hide-sm text-light">Delete</th>
            <th />
          </tr>
        </thead>
        <tbody className='text-light'>{streamList}</tbody>
      </table>
    </Fragment>
  );
};


Stream.propTypes = {
  stream: PropTypes.array.isRequired,
  deleteStreams: PropTypes.func.isRequired
};


export default connect(
  null,
  { deleteStream }
)(Stream);