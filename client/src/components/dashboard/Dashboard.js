import React, { Fragment, useEffect } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../layout/Spinner';
import DashboardActions from './DashboardActions';
import Stream from './Stream';
import { getCurrentProfile, deleteAccount } from '../../actions/profile';


//dashboard displays profile info or requests to set up info
const Dashboard = ({
  getCurrentProfile,
  deleteAccount,
  auth: { user },
  profile: { profile, loading }
}) => {
  useEffect(() => {
    getCurrentProfile();
  }, [getCurrentProfile]);
  
  
  return loading && profile === null ? (
    <Spinner />
  ) : (
    <Fragment>
      <h1 className='large text-light'>Dashboard</h1>
      <p className='lead'>
        <i className='fas fa-user'/> Welcome {user && user.name}
      </p>
      {profile !== null ? (
        <Fragment>
          <DashboardActions />
          <Stream stream={profile.stream}/>
          <div className='my-2'>
            <button className='btn btn-danger' onClick={() => deleteAccount()}>
              Delete Account
            </button>
          </div>
        </Fragment>
      ) : (
        <Fragment>
          <p>TELL THE WORLD WHO YOU ARE</p>
          <Link to='/create-profile' className='btn btn-dark my-1'>
            Create Profile
          </Link>
        </Fragment>
      )}
    </Fragment>
  );
};


Dashboard.propTypes = {
  getCurrentProfile: PropTypes.func.isRequired,
  deleteAccount: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired
};


const mapStateToProps = state => ({
  auth: state.auth,
  profile: state.profile
});


export default connect(
  mapStateToProps,
  { getCurrentProfile, deleteAccount }
)(Dashboard);
