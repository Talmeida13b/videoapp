import React from 'react';
import { Link } from 'react-router-dom';

//links to edit profile, add experience or add education
const DashboardActions = () => {
  return (
    <div className='dash-buttons'>
      <Link to='/edit-profile' className='btn btn-dark'>
        <i className='fas fa-user-circle text-danger' /> Edit Profile
      </Link>
      <Link to='/add-stream' className='btn btn-dark'>
        <i className="fas fa-video text-danger" /> Add Stream
      </Link>
      <Link to='/edit-stream' className='btn btn-dark'>
        <i className='fas fa-camera text-danger' /> Add Pictures
      </Link>
    </div>
  );
};

export default DashboardActions;
